# frozen_string_literal: true

HEADERS = {
  'Private-Token' => ENV['PROJECT_ACCESS_TOKEN']
}

API_URL = 'https://gitlab.com/api/v4'.freeze
